<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>TUTERRENO</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <!-- Favicons -->
    <link href="images2/logo.png" rel="icon">

	<link href="styleTable.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Archivo:400,400i,500,500i,600,600i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="./css/boostrap.min.css" rel="stylesheet" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
     
    <!-- Main Stylesheet File -->
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@3/dark.css" rel="stylesheet">
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js"></script>
    <link href="styleVista.css" rel="stylesheet">
 <!-- Global site tag (gtag.js) - Google Analytics -->
    
    <style>

        .login{
            padding-top:10px !important;
            padding-right:15px !important;
            margin-bottom: 65px !important;
            text-align: right;
            font-size: 12pt;
            color: white!important;
            font-weight: bold;
            text-transform: uppercase;
        }

        .dropdown {
            position: relative;
            display: inline-block;
            width: 30px;
            text-align: right;
            margin-right:15px;
            z-index: 1000;
        }

        .dropdown-content {
            visibility: hidden;
            position: absolute;
            color: white;
            width: 200px;
            right: 0;
            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
            padding: 12px 16px;
            z-index: 4;
            background-color: #2057A4;
            font-weight: normal;
            text-transform: capitalize;
            line-height: 1.5;
            border-radius: 10px;
        }

        .dropdown:hover .dropdown-content {
            visibility: visible;
        }

        .tooltip {
            position: relative;
            display: inline-block;
        }

        .tooltip .tooltiptext {
            visibility: hidden;
            padding:5px;
            width: 120px;
            background-color: #2057A4;
            color: #fff;
            text-align: center;
            border-radius: 6px;
            padding: 5px 0;
            font-size: 8pt !important;
            /* Position the tooltip */
            position: absolute;
            top:35px;
            right:10px;
            z-index: 1;
        }

        .tooltip:hover .tooltiptext {
            visibility: visible;
        }

    </style>
    <style>
        td img{
          width: 15%;
        }
        .arrow_class{
            width:10%;
            margin-bottom:25px;
        }
        @media(max-width:767px){
            .btn.btn-header{
                margin-bottom: 15px;
            }
            .arrow_class{
            width:20%;
            }

            #modal-map{
                width: 330px;
                display: none;
            }
        }
    </style>
    <!--style>

        .tipos-table__row.asd{
            display:none;
        }

        .tipos-table__row div{
            display:none;
        }

        .tipos-table__row.asd2{
            display:none;
        }
    </style-->
</head>
<!--/head-->

<body id="home" class="homepage">
    <div class="body-wrapper">
        <!-- header start -->
        <header class="header">

            <div class="container container-welcome">
                <a href="#" class="logo">
                    <img src="images2/logo.png" alt="" class="logo__img">
                </a>

                <h1 class="header__heading">COTIZAMOS TU TERRENO</h1>
                <hr class="horizontal-line">
                <div class="form" id="buscarDir">
                      <input id="btnSearch" name="btnSearch" placeholder="Ingresá la dirección de tu terreno..." class="form__search" >
                      <div class="form__search-btn2"></div>
                      <div class="form__search-btn" data-toggle="modal" data-target="#exampleModal"></div>
                </div>
                <h6 class="header__sub-heading" style="font-size:11px;color:red;">Solo disponible para CABA</h6>
                <!--button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Launch demo modal
                </button-->
                <div >
                    <a href="#nav"><img src="./images2/downok.gif"  class="arrow_class"></a>
                </div>

            </div>

            <nav class="nav" id="nav">
                <div class="container">
                    <ul class="nav__list">
                        <li class="nav__listitem">
                            <a href="#video" class="nav__listlink">CÓMO FUNCIONA</a>
                        </li>
                        <li class="nav__listitem">
                            <a href="#questions" class="nav__listlink">FAQs</a>
                        </li>
                        <li class="nav__listitem nav__listitem--mb0">
                            <a href="#contacto" class="nav__listlink nav__listlink--border-none">CONTACTO</a>
                        </li>
                    </ul>
                    <div class="nav__toggler" style="margin-left: 43%;">
                        <span class="nav__icone"></span>
                        <span class="nav__icone"></span>
                        <span class="nav__icone nav__icone--mb0"></span>
                    </div>
                </div>
            </nav>
        </header>
        <!-- header end -->
        <!-- video -->
        <section id="video"  onclick="$('.media__video').css('visibility','visible');$('.media__video').get(0).play()" >

                <video controls poster="video/poster.jpeg" class="media__video" controlslist="nodownload">
                    <source src="video/video.mp4" type="video/mp4">
                </video>

        </section>


        <!-- end video -->
        <!-- questions -->
        <section class="questions" id="questions">
            <div class="container">
                <h1 class="questions__heading">PREGUNTAS FRECUENTES</h1>
                <div class="questions__wrapper">
                    <ul class="questions__list">
                        <li class="accordian">
                            <div class="accordian__icone">¿Qué es Tu Terreno?</div>
                            <p class="accordian__para">
                                Cotizamos Terrenos.
                             </p>
                            <hr class="accordian__devider">
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <!-- questions end -->
    
        <!-- customer end -->
 
        <!-- credit end -->
        <!-- contact -->
        <section class="contact">
            <div class="container">
                <h1 class="contact__heading">CONTACTANOS</h1>
                <a href="#" class="contact__link contact__link--mgb">
                    <span class="contact__img">
                        <img src="./images2/place2.png" alt="">
                    </span>
                    <span class="contact__text">info@tuterreno.com.ar</span>
                </a>
            </div>
            <div id="googleMap" style="display:none;"></div>
        </section>
        <!-- contact end -->
        <footer class="footer">
            <div class="container">
                <p class="footer__para"></p>
                <p class="footer__para"></p>
                <p class="footer__para footer__para--mgb"></p>
                <ul class="footer-list footer-list--mgb">
                    <li class="footer-list__item footer-list__item-mb">
                        <a href="./terminos_Condiciones" target="_blank" class="footer-list__link footer-list__link--mgr">Términos y condiciones</a>
                    </li>
                    <li class="footer-list__item footer-list__item-mb">

                    </li>
                </ul>
                <ul class="footer-list">
               
                </ul>
            </div>
        </footer>
        <!-- footer end -->
    </div>



<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
         <!--button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button-->
      <div style="height:0px;" id="divRaro">
         <center><div id="spinnerModal"><img src="./images/spinner-icon-gif-19.jpg" id="spinnerModal"></div></center>
         <center><div id="message" style="margin-top:20px;">                   
         </div>
         </center>
     
            <form id="formModal"  action="sendemail.php" style="display:none;margin:20px;" method="POST" >
                 <div class="row">
					<div class="col-md-6">
						<h3 class="hidden-xs hidden-sm">Tu cotización :</h3>
                        <input type="hidden" value="" name="precio_venta" id="precio_venta">
                        <input type="hidden" value="" name="plusvalia" id="plusvalia">
                        <input type="hidden" value="" name="mtsVendibles" id="mtsVendibles">
                        <input type="hidden" value="" name="direccion" id="direccion">
                        <input type="hidden" value="" name="barrio" id="barrio">
						<h6 style="background:#EF5E4E;border-radius: 12px;margin:10px;" class="blured" id="id_valor"><p class="preciomodal" style="color: #fff;padding: 3px;">Valor de la propiedad: <span id="precio"></span></p></h6>
				
						<p class="forgot-password-description" style="font-size: 10px !important; margin-bottom: 0;">*Tu cotización es en base a un proceso automático. El valor puede variar de acuerdo al valor comercial de la cuadra.</p>

			 			<!--<p class="forgot-password-description" style="font-size: 10px !important;line-height: 12px">**La cantidad de Mts<sup>2</sup> calculada es a través de un proceso automático basado en el reglamento del Código de Planeamiento Urbano del GCBA (No incluye enrase, premios o solicitudes especiales).</p>-->
			 			<p class="descmodal" style="font-weight: bold;margin-bottom: 0!important;">Si estas interesado en vender tu terreno, dejanos tus datos a continuación y nos comunicaremos a la brevedad.</p>
			 			<h6><p class="contmodal hidden-xs hidden-sm" style="color:white;">Contacto:</p></h6>
					</div>
					<div class="col-md-6 hidden-xs hidden-sm">
					    <div id="modal-map" style="width:280px;height:200px;">    <img style="background-color: red;border:1px solid black;width:100%;margin:20px 0 20px 0;" src="https://maps.googleapis.com/maps/api/staticmap?center=-34.55265422816,-58.449975646404&amp;zoom=16&amp;size=327x230&amp;maptype=places&amp;markers=color:red|size:small|-34.55265422816,-58.449975646404&amp;key=AIzaSyCGlT4HqvbfYNafDJ_3-11BS7N7buLA7ZQ"></div>
					</div>
				</div>
                <div class="row">
                    <div class="col-md-12">
                        <h6><p class="contmodal hidden-xs hidden-sm">Contacto:</p></h6>
                    </div>
                </div>    
                <center><div class="row">
					<div class="col-md-6">  
                        <br>
						<div class="form-elem input-box color-focus-black">
							<div class="elem-icon"><i class="icon-user"></i></div>
							<input type="text" name="nombre" id="nombre" placeholder="Nombre y Apellido" value="" required>
						</div>

						<div class="form-elem input-box color-focus-black">
							<div class="elem-icon"><i class="icon-envelope"></i></div>
							<input type="email" name="email" id="email" placeholder="Email" value="" required>
						</div>

						<div class="form-elem input-box color-focus-black">
							<div class="elem-icon"><i class="icon-phone"></i></div>
							<input type="number" name="CodCel" id="CodCel"  style="width:49px;" placeholder="011" maxlength="3" required><input type="number" style="width:163px;" name="Telefono" id="inputTelefono" placeholder="33445566" maxlength="8" required>
						</div>
                    

                        <div class="form-elem input-box color-focus-black">
                                <br>
                            	<h6 class="hidden-xs hidden-sm">Horario de llamada :</h6>
                                <br>
							<div class="elem-icon"><i class="icon-map"></i></div>
							<input type="time" name="desde" id="desde" placeholder="DESDE" min="10:00" max="16:00" step="600" style="width:100px;" >
                            <input type="time" name="hasta" id="hasta" placeholder="HASTA" min="12:00" max="18:00" step="600"style="width:100px;" >
						</div> 
						
					</div>
					<div class="col-md-6">
                        <br>
                        <div class="form-elem input-box color-focus-black">
                           	<h6 class="hidden-xs hidden-sm">Es Propietario? :</h6>
							<div class="elem-icon"><i class="icon-map"></i></div>
                            <div class="form-check form-check-inline">
                                 <input class="form-check-input" type="radio" name="propietario" id="inlineRadio1" value="si">
                                <label class="form-check-label" for="inlineRadio1">SI</label> &nbsp;&nbsp; 
                                <input class="form-check-input" type="radio" name="propietario" id="inlineRadio2" value="no">
                                <label class="form-check-label" for="inlineRadio2">NO</label>
                            </div>
						</div> 
						<div class="form-elem input-box color-focus-black">
							<div class="elem-icon"><i class="icon-map"></i></div>
							<input type="text"  id="inputDireccion" placeholder="Dirección del terreno" disabled>
						</div>
                        
                    	<div class="form-elem input-box color-focus-black">
							<div class="elem-icon"><i class="icon-location-pin"></i></div>
							<input type="text"  id="inputBarrio" placeholder="Barrio" disabled>
						</div>
						<label style="font-size: 12px;color: #000 !important;"><input type="checkbox" class="form-check-inline" id="terminos" onclick="verificarTerminos();">
                        <a class="btn-terminos" href="" target="_blank">Acepto los términos y condiciones</a></label>

                       
					</div>
                   

				</div><center>
                 <br>   
                <!--div class="form-check">
                    <input type="checkbox" class="form-check-inline" id="terminos" onclick="verificarTerminos();">
                    <label class="form-check-label" style="size:9px;" for="terminos">Acepto los terminos y condiciones</label>

                </div-->
                <button type="submit" id="aceptar" class="btn btn-primary btn-block" disabled>CONTACTATE</button>	
                <!--button type="button" id="aceptar1" style="visibility: hidden;" class="btn btn-primary btn-block" data-dismiss="modal">CERRAR</button-->	
        </form> 
      </div>
     </div>
  </div>
</div>

    <!--
    <script src="js/jquery.js"></script>
    -->
    <script
        src="https://code.jquery.com/jquery-3.5.0.min.js"
        integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ="
        crossorigin="anonymous"></script>


    <script>
        $(".accordian__icone").on("click", function() {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                $(this).next('.accordian__para').removeClass('active');
            } else {
                $(this).addClass('active');
                $(this).next('.accordian__para').addClass('active');
            }
        });



        $(".form__search-btn").on("click", function() {
            let direccion = $('#btnSearch').val();

            normalizar(direccion, true);
        });



        $('.form__search').on('keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                $(".form__search-btn").trigger("click");
            }
        });



        function normalizar(dir) {
           //let URL = `http://127.0.0.1:8000/api/normalizarValidar`;
           let URL = `https://www.factibilidades.com.ar/api/data/`+$('#btnSearch').val();

            if (dir.length < 9 ) {
                        $("#message").html('LA DIRECCION NO EXISTE  <button type="button" id="cerrar" data-dismiss="modal" class="btn btn-primary btn-block">CERRAR</button>');
                        $('#spinnerModal').hide();
                        $('#divRaro').css('height','auto')
                        return false;
            }

            axios.interceptors.request.use(x=>{
                console.log("cargando...");
                $('#divRaro').css('height','0px')
                $('.form__search-btn').hide();
                $('#spinnerModal').show();
                $('#formModal').hide();
                $("#message").hide()

             

                return x;
            });

            axios.get(URL, {
                    
                }, {
                    headers: {
                        'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': '*'
                    }
                }).then((res) => {
                   
                    if (typeof res.data.data !== 'object'){
                        console.log("jp");
                        $("#message").html(res.data.data + ' <button type="button" id="cerrar" data-dismiss="modal" class="btn btn-primary btn-block">CERRAR</button>');
                        $("#message").show();
                        $('#spinnerModal').hide();
                        $('.form__search-btn').show();
                        $('#divRaro').css('height','auto')
                        return false;
                    }
                    $('#divRaro').css('height','auto')
                    $("#message").hide();
                    $('.form__search-btn').show();
                    $('#spinnerModal').hide();
                    $('#formModal').show();
                    $('#inputDireccion').val($('#btnSearch').val());
                    $('#direccion').val($('#btnSearch').val());
                    $('#inputBarrio').val(res.data.data.datos_utiles.barrio);
                    $('#barrio').val(res.data.data.datos_utiles.barrio);
                    $('#precio').html('  US$ '+new Intl.NumberFormat("de-DE").format(res.data.data.precio));
                    $('#precio_venta').val('US$ '+new Intl.NumberFormat("de-DE").format(res.data.data.precio));
                    $('#plusvalia').val('AR$ '+new Intl.NumberFormat("de-DE").format(res.data.data.plusvalia));
                    $('#mtsVendibles').val(res.data.data.mtsVendibles);
                    

                })
                .catch((e) => {
                    //catch
                    console.error("ERROR ", e);
                }).then(() => {
                    //props.history.push("/welcome");
                    //finalmente
                })
        }

        function verificarTerminos(){
            if($('#aceptar').prop('disabled')==true)
            {
                $('#aceptar').prop('disabled', false);
            }else{
                
                $('#aceptar').prop('disabled', true);
            }
        }

         
            
    </script>
    <script>
        $(document).ready(function(){
            $("input[value='COMPRAR']").click(function(e){
                e.preventDefault();
                let formulario=document.getElementById("formCreditos");
                var radioValue = $("input[type='radio']:checked").val();

                formulario.setAttribute('action', radioValue)
                if(radioValue!=undefined){
                    formulario.submit();
                }


            });


        });
    </script>

</body>

</html>
